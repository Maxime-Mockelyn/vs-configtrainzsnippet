# Change Log

All notable changes to the "configtrainzsnippet" extension will be documented in this file.

## [Unreleased]

## [1.0.4]

[Added]
- Support 4.2
    - Bogey

## [1.0.3]

[Added]
- Support 3.7
    - Sound

[Modified]

- Support 4.2
    - Indentation Corriger
    - Scenery Lod

## [1.0.2]

[Added]
- Indentation des fichier de configuration

## [1.0.1]

[Added]
- Prise en charge de la version 3.5 de Trainz
    - Scenery   

## [1.0.0]

- Publication initial du snippet de Config Trainz pour VSCode
